package com.abd.androidlogger.sample

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.abd.androidlogger.ABDLogger

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ABDLogger.init(this, printLogs = true)

        val tag = "MainActivity"
        val button = findViewById<Button>(R.id.button)
        button.setOnClickListener {
            ABDLogger.v(tag, "VERBOSE")
            ABDLogger.d(tag, "DEBUG")
            ABDLogger.i(tag, "INFO")
            ABDLogger.w(tag, "WARN")
            ABDLogger.e(tag, "ERROR")

            ABDLogger.d("tag1", "Single Tag")
            ABDLogger.d("tag1, tag2, tag3", "Multiple Tags")
            ABDLogger.d("Activity Tracking  , Yow     ", "Tags with space")
            ABDLogger.d("Activity Tracking", "DEBUG")
            ABDLogger.d("PMCLogger", "DEBUG")
            ABDLogger.d("MainActivity", "Tags with space")
            ABDLogger.d("PMCLogger", "Tags with space")

            ABDLogger.i("PMCLogger", "Single Tag")
            ABDLogger.i("MainActivity", "Single Tag")

            ABDLogger.i("", "Untagged Log")

            ABDLogger.viewLogs(this)
        }
    }
}
