package com.abd.androidlogger

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.abd.androidlogger.databinding.ListItemPmclogBinding

class ABDLogAdapter : ListAdapter<ABDLog, ABDLogAdapter.ViewHolder>(
    object : DiffUtil.ItemCallback<ABDLog>() {
        override fun areItemsTheSame(oldItem: ABDLog, newItem: ABDLog): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ABDLog, newItem: ABDLog): Boolean {
            return oldItem.msg == newItem.msg
        }
    }) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ListItemPmclogBinding>(
            LayoutInflater.from(parent.context),
            R.layout.list_item_pmclog,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(
        private val binding: ListItemPmclogBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(log: ABDLog) {
            binding.textviewId.text = "#${log.id}"
            binding.textviewTimestamp.text = log.timestamp
            binding.textviewTags.text = log.tag
            binding.textviewPriority.text = ABDLogger.getPriorityText(log.priority)
            binding.textviewMessage.text = log.msg
        }
    }
}

