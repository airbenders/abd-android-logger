package com.abd.androidlogger

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [ABDLog::class], version = 2)
abstract class ABDLogDatabase : RoomDatabase() {

    abstract fun logDao(): ABDLogDao

    companion object {
        @Volatile
        private var INSTANCE: ABDLogDatabase? = null

        fun getDatabase(context: Context): ABDLogDatabase {
            val tempInstance = INSTANCE

            if (tempInstance != null) return tempInstance

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ABDLogDatabase::class.java,
                    "abdlog_database"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                return instance
            }
        }
    }
}