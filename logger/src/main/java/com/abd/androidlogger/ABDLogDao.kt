package com.abd.androidlogger

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ABDLogDao {

    @Insert
    fun addLog(log: ABDLog)

    @Query("SELECT tag FROM ABDLog GROUP BY tag")
    fun getAllTags(): List<String>

    @Query("DELETE FROM ABDLog")
    fun deleteAllLogs()

    @Query("SELECT * FROM ABDLog WHERE priority >= :priority AND tag LIKE :tag AND msg LIKE :msg ORDER BY id ASC")
    fun getAllLogs(priority: Int, tag: String, msg: String): List<ABDLog>
}